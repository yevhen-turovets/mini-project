import Vue from 'vue';
import { commentMapper } from '@/services/Normalizer';
import {
    SET_COMMENTS,
    ADD_COMMENT,
    DELETE_COMMENT,
    SET_COMMENT_IMAGE,
    SET_COMMENT,
    DISLIKE_COMMENT,
    LIKE_COMMENT
} from './mutationTypes';


export default {
    [SET_COMMENTS]: (state, comments) => {
        let commentsByIdMap = {...state.comments};

        comments.forEach(comment => {
            commentsByIdMap = {
                ...commentsByIdMap,
                [comment.id]: commentMapper(comment)
            };
        });

        state.comments = commentsByIdMap;
    },

    [ADD_COMMENT]: (state, comment) => {
        state.comments = {
            ...state.comments,
            [comment.id]: commentMapper(comment)
        };
    },

    [SET_COMMENT_IMAGE]: (state, { id, imageUrl }) => {
        state.comments[id].imageUrl = imageUrl;
    },

    [SET_COMMENT]: (state, comment) => {
        state.comments = {
            ...state.comments,
            [comment.id]: commentMapper(comment)
        };
    },

    [DELETE_COMMENT]: (state, id) => {
        Vue.delete(state.comments, id);
    },

    [LIKE_COMMENT]: (state, { id, userId }) => {
        state.comments[id].likesCount++;

        state.comments[id].likes.push({ userId });
    },

    [DISLIKE_COMMENT]: (state, { id, userId }) => {
        state.comments[id].likesCount--;

        state.comments[id].likes = state.comments[id].likes.filter(like => like.userId !== userId);
    }
};
