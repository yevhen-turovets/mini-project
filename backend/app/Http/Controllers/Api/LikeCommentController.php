<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Action\Comment\LikeCommentAction;
use App\Action\Comment\LikeCommentRequest;
use App\Action\GetCollectionRequest;
use App\Action\Comment\GetLikedUsersIdByCommentIdAction;
use App\Action\Comment\GetLikedUsersIdByCommentIdRequest;
use App\Action\User\GetUserCollectionByArrIdAction;
use App\Http\Controllers\ApiController;
use App\Http\Presenter\UserArrayPresenter;
use App\Http\Request\Api\CollectionHttpRequest;
use App\Http\Response\ApiResponse;

final class LikeCommentController extends ApiController
{
    private $likeCommentAction;
    private $getLikedUsersIdByCommentIdAction;
    private $getUserCollectionByArrIdAction;
    private $presenter;

    public function __construct(
        LikeCommentAction $likeCommentAction,
        GetLikedUsersIdByCommentIdAction $getLikedUsersIdByCommentIdAction,
        GetUserCollectionByArrIdAction $getUserCollectionByArrIdAction,
        UserArrayPresenter $presenter
    ) {
        $this->likeCommentAction = $likeCommentAction;
        $this->getLikedUsersIdByCommentIdAction = $getLikedUsersIdByCommentIdAction;
        $this->getUserCollectionByArrIdAction = $getUserCollectionByArrIdAction;
        $this->presenter = $presenter;
    }

    public function likeOrDislikeComment(string $id): ApiResponse
    {
        $response = $this->likeCommentAction->execute(new LikeCommentRequest((int)$id));

        return $this->createSuccessResponse(['status' => $response->getStatus()]);
    }

    public function getListUsersWhoLikedByCommentId(string $id, CollectionHttpRequest $request): ApiResponse
    {
        $usersId = $this->getLikedUsersIdByCommentIdAction->execute(
            new GetLikedUsersIdByCommentIdRequest((int)$id)
        );

        $response = $this->getUserCollectionByArrIdAction->execute($usersId);

        return $this->createSuccessResponse($this->presenter->presentCollection($response));
    }
}
