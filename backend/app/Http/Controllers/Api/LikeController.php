<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Action\Tweet\LikeTweetAction;
use App\Action\Tweet\LikeTweetRequest;
use App\Action\Tweet\GetLikedUsersIdByTweetIdAction;
use App\Action\Tweet\GetLikedUsersIdByTweetIdRequest;
use App\Action\User\GetUserCollectionByArrIdAction;
use App\Http\Controllers\ApiController;
use App\Http\Response\ApiResponse;
use App\Http\Presenter\UserArrayPresenter;

final class LikeController extends ApiController
{
    private $likeTweetAction;
    private $getLikedUsersIdByTweetIdAction;
    private $getUserCollectionByArrIdAction;
    private $presenter;

    public function __construct(
        LikeTweetAction $likeTweetAction,
        GetLikedUsersIdByTweetIdAction $getLikedUsersIdByTweetIdAction,
        GetUserCollectionByArrIdAction $getUserCollectionByArrIdAction,
        UserArrayPresenter $presenter
    ) {
        $this->likeTweetAction = $likeTweetAction;
        $this->getLikedUsersIdByTweetIdAction = $getLikedUsersIdByTweetIdAction;
        $this->getUserCollectionByArrIdAction = $getUserCollectionByArrIdAction;
        $this->presenter = $presenter;
    }

    public function likeOrDislikeTweet(string $id): ApiResponse
    {
        $response = $this->likeTweetAction->execute(new LikeTweetRequest((int)$id));

        return $this->createSuccessResponse(['status' => $response->getStatus()]);
    }

    public function getListUsersWhoLikedByTweetId(string $id): ApiResponse
    {
        $usersId = $this->getLikedUsersIdByTweetIdAction->execute(
            new GetLikedUsersIdByTweetIdRequest((int)$id)
        );

        $response = $this->getUserCollectionByArrIdAction->execute($usersId);

        return $this->createSuccessResponse($this->presenter->presentCollection($response));
    }
}
