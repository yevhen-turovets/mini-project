<?php

declare(strict_types=1);

namespace App\Action\User;

use App\Repository\UserRepository;

final class GetUserCollectionByArrIdAction
{
    private $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function execute($userId)
    {
        return $this->repository->getUsersByIds($userId);
    }
}
