<?php

declare(strict_types=1);

namespace App\Action\Comment;

use App\Entity\Like;
use App\Mail\CommentLikeEmail;
use App\Repository\LikeRepository;
use App\Repository\CommentRepository;
use Illuminate\Mail\Mailer;
use Illuminate\Support\Facades\Auth;

final class LikeCommentAction
{
    private $commentRepository;
    private $likeRepository;
    private $mailer;

    private const ADD_LIKE_STATUS = 'added';
    private const REMOVE_LIKE_STATUS = 'removed';

    public function __construct(CommentRepository $commentRepository, LikeRepository $likeRepository, Mailer $mailer)
    {
        $this->commentRepository = $commentRepository;
        $this->likeRepository = $likeRepository;
        $this->mailer = $mailer;
    }

    public function execute(LikeCommentRequest $request): LikeCommentResponse
    {
        $comment = $this->commentRepository->getById($request->getCommentId());

        $userId = Auth::id();

        // if user already liked comment, we remove previous like
        if ($this->likeRepository->existsForCommentByUser($comment->id, $userId)) {
            $this->likeRepository->deleteForCommentByUser($comment->id, $userId);

            return new LikeCommentResponse(self::REMOVE_LIKE_STATUS);
        }

        $like = new Like();
        $like->forComment(Auth::id(), $comment->id);

        $this->likeRepository->save($like);

        $user = $comment->author->email;

        $this->mailer->to($user)->send(new CommentLikeEmail());

        return new LikeCommentResponse(self::ADD_LIKE_STATUS);
    }
}
