<?php

declare(strict_types = 1);

namespace App\Action\Auth;

use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

final class SendResetPasswordAction extends ApiController
{
    use SendsPasswordResetEmails;

    public function execute($request)
    {
        return $this->sendResetLinkEmail($request);
    }

    protected function sendResetLinkResponse(Request $request, $response)
    {
        return $this->createSuccessResponse([
            'message' => 'Password reset email sent.',
            'data' => $response
             ]);
    }

    protected function sendResetLinkFailedResponse(Request $request, $response)
    {
        return $this->createErrorResponse('Email could not be sent to this email address.', '401');
    }
}
