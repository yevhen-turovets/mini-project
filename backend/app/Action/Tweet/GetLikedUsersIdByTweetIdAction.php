<?php

declare(strict_types=1);

namespace App\Action\Tweet;

use App\Repository\LikeRepository;

final class GetLikedUsersIdByTweetIdAction
{
    private $likeRepository;

    public function __construct(LikeRepository $likeRepository)
    {
        $this->likeRepository = $likeRepository;
    }

    public function execute(GetLikedUsersIdByTweetIdRequest $request)
    {
        return $this->likeRepository->getUsersIdByTweetId($request->getTweetId());
    }
}
