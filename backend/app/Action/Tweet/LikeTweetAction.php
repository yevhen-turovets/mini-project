<?php

declare(strict_types=1);

namespace App\Action\Tweet;

use App\Entity\Like;
use App\Mail\TweetLikeEmail;
use App\Repository\LikeRepository;
use App\Repository\TweetRepository;
use Illuminate\Mail\Mailer;
use Illuminate\Support\Facades\Auth;

final class LikeTweetAction
{
    private $tweetRepository;
    private $likeRepository;
    private $mailer;

    private const ADD_LIKE_STATUS = 'added';
    private const REMOVE_LIKE_STATUS = 'removed';

    public function __construct(TweetRepository $tweetRepository, LikeRepository $likeRepository, Mailer $mailer)
    {
        $this->tweetRepository = $tweetRepository;
        $this->likeRepository = $likeRepository;
        $this->mailer = $mailer;
    }

    public function execute(LikeTweetRequest $request): LikeTweetResponse
    {
        $tweet = $this->tweetRepository->getById($request->getTweetId());

        $userId = Auth::id();

        // if user already liked tweet, we remove previous like
        if ($this->likeRepository->existsForTweetByUser($tweet->id, $userId)) {
            $this->likeRepository->deleteForTweetByUser($tweet->id, $userId);

            return new LikeTweetResponse(self::REMOVE_LIKE_STATUS);
        }

        $like = new Like();
        $like->forTweet(Auth::id(), $tweet->id);

        $this->likeRepository->save($like);

        $user = $tweet->author->email;

        $this->mailer->to($user)->send(new TweetLikeEmail());

        return new LikeTweetResponse(self::ADD_LIKE_STATUS);
    }
}
